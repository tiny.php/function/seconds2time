# Función seconds2time

## Instalación via composer

```bin
composer require tinyphp-function/seconds2time
```

## Funciones

```php
/** Ejecuta todos los archivos de un directorio y sus subdirectorios */
function seconds2time(int $sec): string
```
