<?php
/**
 * function.php
 * /
 * 
 * @package tinyphp-function/seconds2time
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2024, J&S Perú <https://jys.pe>
 * @created 2024-11-15 18:57:20
 * @version 20241115185826 (Rev. 8)
 * @license MIT
 * @filesource
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

if (!function_exists('seconds2time'))
{
    /**
     * Convierte segundos a un formato de tiempo.
     *
     * Esta función toma una cantidad de segundos y los convierte a un formato de tiempo
     * legible, indicando días si el total de segundos excede las 24 horas.
     *
     * @param int $sec La cantidad de segundos a convertir.
     * @return string Una cadena en formato "d días HH:MM:SS" o "HH:MM:SS" si no hay más de un día.
     */
    function seconds2time(int $sec): string
    {
        $day = floor($sec / 86400);
        $hrs = floor(($sec % 86400) / 3600);
        $min = floor(($sec % 3600) / 60);
        $sec %= 60;

        if ($day > 0)
            return sprintf("%d día%s %02d:%02d:%02d", $day, $day > 1 ? 's' : '', $hrs, $min, $sec);

        return sprintf("%02d:%02d:%02d", $hrs, $min, $sec);
    }
}
